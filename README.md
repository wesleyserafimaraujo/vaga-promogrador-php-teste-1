# Oba, você conseguiu chegar aqui... #

O primeiro teste consiste numa implementação bem simples, porém exige um certo nível de raciocínio lógico para ser resolvido da melhor forma.

### Implemente um algorítimo para resolver a seguinte questão ###

Temos 1 valor devido e um valor pago, precisamos que seja calculado o troco e que esse troco seja retornado usando o menor número de notas. As notas disponíveis são:
#### 100, 50, 20, 10, 5, 2 e 1 ####
Desconsiderar valores fracionados(centavos) para esse teste.

### Exemplo ###
* Valor devido: 80,00
* Valor pago: 100,00
* Troco: 20,00
* Notas: 1 nota de 20,00

* Valor devido: 32,00
* Valor pago: 50,00
* Troco: 18,00
* Notas: 1 nota de 10,00, 1 nota de 5,00, 1 nota de 2,00 e 1 nota de 1,00

### Recomendações ###
* Faça um fork desse repositório e após concluir a tarefa faça um pull request.
* Implementar em PHP e considerar usar as features do PHP 7 caso se apliquem na resolução do problema.
* Documentar o código corretamente (comentários)
* Atenção nas boas práticas de programção e usar as recomendações da PSR-2
* Se possível implemetar o mesmo algorítimo em JS (Não obrigatório)

